package customer;
 
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
 
@Stateless
public class CustomerDao {
    // Injected database connection:
    @PersistenceContext private EntityManager em;

    // Stores a new customer:
    public void persist(Customer customer) {
        em.persist(customer);
    }

    // Retrieves all the customers:
    public List<Customer> getAllCustomers() {
        TypedQuery<Customer> query = em.createQuery(
            "SELECT g FROM Customer g ORDER BY g.id", Customer.class);
        return query.getResultList();
    }
}