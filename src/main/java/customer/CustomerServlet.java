package customer;
 
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet(name="CustomerServlet", urlPatterns={"/customer"})
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    // Injected DAO EJB:
    @EJB CustomerDao customerDao;

    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Display the list of customers:
        request.setAttribute("customers", customerDao.getAllCustomers());
        request.getRequestDispatcher("/customer.jsp").forward(request, response);
    }

    @Override
    protected void doPost(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Handle a new customer:
        String name = request.getParameter("name");
        if (name != null)
            customerDao.persist(new Customer(name));

        // Display the list of customers:
        doGet(request, response);
    }
}