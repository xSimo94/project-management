/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author simonesaleri
 */
@Stateless
public class EmployeeDao {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    // Injected database connection:
    @PersistenceContext private EntityManager em;
    
    // Stores a new employee: 
    public void persist(Employee employee) {
        em.persist(employee);
    }

    // Retrieves all the guests:
    public List<Employee> getAllEmployees() {
        TypedQuery<Employee> query = em.createQuery(
            "SELECT e FROM Employee e ORDER BY e.id", Employee.class);
        return query.getResultList();
    }
}
