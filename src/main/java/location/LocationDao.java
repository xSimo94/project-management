package location;
 
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
 
@Stateless
public class LocationDao {
    // Injected database connection:
    @PersistenceContext private EntityManager em;

    // Stores a new location:
    public void persist(Location location) {
        em.persist(location);
    }

    // Retrieves all the locations:
    public List<Location> getAllLocations() {
        TypedQuery<Location> query = em.createQuery(
            "SELECT g FROM Location g ORDER BY g.id", Location.class);
        return query.getResultList();
    }
}