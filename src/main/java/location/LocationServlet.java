package location;
 
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet(name="LocationServlet", urlPatterns={"/location"})
public class LocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    // Injected DAO EJB:
    @EJB LocationDao locationDao;

    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Display the list of locations:
        request.setAttribute("locations", locationDao.getAllLocations());
        request.getRequestDispatcher("/location.jsp").forward(request, response);
    }

    @Override
    protected void doPost(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Handle a new location:
        String name = request.getParameter("name");
        if (name != null)
            locationDao.persist(new Location(name));

        // Display the list of locations:
        doGet(request, response);
    }
}