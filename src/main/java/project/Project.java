/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author simonesaleri
 */
@Entity
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;

    // Constructors:
    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    Project(String name) {
        this.name = name;
    }

    // String Representation:
    @Override
    public String toString() {
        return "Titolo: " + name +", descrizione: " + description;
    }
    
}
