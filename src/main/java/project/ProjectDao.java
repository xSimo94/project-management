/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author simonesaleri
 */
@Stateless
public class ProjectDao {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    // Injected database connection:
    @PersistenceContext private EntityManager em;
    
    // Stores a new project: 
    public void persist(Project project) {
        em.persist(project);
    }

    // Retrieves all the guests:
    public List<Project> getAllProjects() {
        TypedQuery<Project> query = em.createQuery(
            "SELECT e FROM Project e ORDER BY e.id", Project.class);
        return query.getResultList();
    }
}
