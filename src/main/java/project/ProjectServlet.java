/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simonesaleri
 */
@WebServlet(name = "ProjectServlet", urlPatterns = {"/project"})
public class ProjectServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // Injected DAO EJB:
    @EJB ProjectDao projectDao;

    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Display the list of projects:
        request.setAttribute("projects", projectDao.getAllProjects());
        request.getRequestDispatcher("/project.jsp").forward(request, response);
    }

    @Override
    protected void doPost(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Handle a new project:
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        if (name != null)
            if (description != null){
                projectDao.persist(new Project(name, description));
            }
            else {
                projectDao.persist(new Project(name));
            }
        // Display the list of Projects:
        doGet(request, response);
    }
}
