<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*,project.Project"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>JPA Projectbook Web Application Tutorial</title>
    </head>
 
    <body>
        <form method="POST" action="project">
            Name: <input type="text" name="name" />
            Description: <input type="text" name="description" />
            <input type="submit" value="Add" />
        </form>
 
        <hr><ol> <%
            @SuppressWarnings("unchecked") 
            List<Project> projects = (List<Project>)request.getAttribute("projects");
            if (projects != null) {
                for (Project project : projects) { %>
                    <li> <%= project %>
                        <form method="POST" action="project">
                            <input name=project.name type="submit" value="Remove" />
                        </form> 
                    </li> <%
                }
            } %>
        </ol><hr>
        
        <a href="index.jsp">Dashboard</a>
     </body>
 </html>