<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*,customer.Customer"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>JPA Customerbook Web Application Tutorial</title>
    </head>
 
    <body>
        <form method="POST" action="customer">
            Name: <input type="text" name="name" />
            <input type="submit" value="Add" />
        </form>
 
        <hr><ol> <%
            @SuppressWarnings("unchecked") 
            List<Customer> customers = (List<Customer>)request.getAttribute("customers");
            if (customers != null) {
                for (Customer customer : customers) { %>
                    <li> <%= customer %>
                        <form method="POST" action="customer">
                            <input name=customer.name type="submit" value="Remove" />
                        </form> 
                    </li> <%
                }
            } %>
        </ol><hr>
        
        <a href="index.jsp">Dashboard</a>
     </body>
 </html>