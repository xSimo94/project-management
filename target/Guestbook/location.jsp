<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*,location.Location"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>JPA Locationbook Web Application Tutorial</title>
    </head>
 
    <body>
        <form method="POST" action="location">
            Name: <input type="text" name="name" />
            <input type="submit" value="Add" />
        </form>
 
        <hr><ol> <%
            @SuppressWarnings("unchecked") 
            List<Location> locations = (List<Location>)request.getAttribute("locations");
            if (locations != null) {
                for (Location location : locations) { %>
                    <li> <%= location %>
                        <form method="POST" action="location">
                            <input name=location.name type="submit" value="Remove" />
                        </form> 
                    </li> <%
                }
            } %>
        </ol><hr>
        
        <a href="index.jsp">Dashboard</a>
     </body>
 </html>