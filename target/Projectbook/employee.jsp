<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*,employee.Employee"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>JPA Employeebook Web Application Tutorial</title>
    </head>
 
    <body>
        <form method="POST" action="employee">
            Name: <input type="text" name="name" />
            <input type="submit" value="Add" />
        </form>
 
        <hr><ol> <%
            @SuppressWarnings("unchecked") 
            List<Employee> employees = (List<Employee>)request.getAttribute("employees");
            if (employees != null) {
                for (Employee employee : employees) { %>
                    <li> <%= employee %>
                        <form method="POST" action="employee">
                            <input name=employee.name type="submit" value="Remove" />
                        </form> 
                    </li> <%
                }
            } %>
        </ol><hr>
        
        <a href="index.jsp">Dashboard</a>
     </body>
 </html>